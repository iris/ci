This is the docker image for Iris CI.

Here's how to update this image on Docker Hub:
```
docker build -t ralfjung/opam-ci:opam2 .
docker login
docker push ralfjung/opam-ci:opam2
```
